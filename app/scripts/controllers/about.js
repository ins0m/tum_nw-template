'use strict';

/**
 * @ngdoc function
 * @name nwAppApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the nwAppApp
 */
angular.module('nwAppApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
