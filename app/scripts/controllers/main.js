'use strict';

/**
 * @ngdoc function
 * @name nwAppApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nwAppApp
 */
angular.module('nwAppApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
